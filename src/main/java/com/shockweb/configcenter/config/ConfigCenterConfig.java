package com.shockweb.configcenter.config;

/**
 * 配置中心的配置
 * 
 * @author 彭明华
 * 2018年3月20日 创建
 */
public class ConfigCenterConfig {

	/**
	 * 当前配置中心服务器的url
	 */
	private String hostUrl = null;
	
	/**
	 * 设置当前配置中心服务器的url
	 * @param hostUrl
	 */
	public void setHostUrl(String hostUrl){
		this.hostUrl = hostUrl;
	}
	
	/**
	 * 当前配置中心服务器的url
	 * @return
	 */
	public String getHostUrl(){
		return hostUrl;
	}
	
	/**
	 * 镜像配置中心url
	 */
	private String mirrorUrl = null;
	
	/**
	 * 设置镜像配置中心url
	 * @param mirrorUrl
	 */
	public void setMirrorUrl(String mirrorUrl){
		this.mirrorUrl = mirrorUrl;
	}
	
	/**
	 * 镜像配置中心url
	 * @return
	 */
	public String getMirrorUrl(){
		return mirrorUrl;
	}
	
	
	/**
	 * 链接到其他配置中心客户端建立连接的超时时间
	 */
	private int clientConnectTimeOut = 5000;
	
	/**
	 * 设置链接到其他配置中心客户端建立连接的超时时间
	 * @param clientConnectTimeOut
	 */
	public void setClientConnectTimeOut(int clientConnectTimeOut){
		this.clientConnectTimeOut = clientConnectTimeOut;
	}
	
	/**
	 * 链接到其他配置中心客户端建立连接的超时时间
	 * @return
	 */
	public int getClientConnectTimeOut() {
		return clientConnectTimeOut;
	}
	
	/**
	 * 发送到其他配置中心请求的客户端请求的超时时间
	 */
    private int clientTimeOut = 5000;
    
	/**
	 * 设置发送到其他配置中心请求的客户端请求的超时时间
	 * @param clientTimeOut
	 */
	public void setClientTimeOut(int clientTimeOut){
		this.clientTimeOut = clientTimeOut;
	}
	
	/**
	 * 发送到其他配置中心请求的客户端请求的超时时间
	 * @return
	 */
	public int getClientTimeOut() {
		return clientTimeOut;
	}


	/**
	 * 客户端等待时扫描时间结果
	 */
    private int clientSleepTime = 20;
    
	/**
	 * 设置客户端等待时扫描时间结果
	 * @param clientSleepTime
	 */
	public void setClientSleepTime(int clientSleepTime){
		this.clientSleepTime = clientSleepTime;
	}
	
	/**
	 * 客户端等待时扫描时间结果
	 * @return
	 */
	public int getClientSleepTime() {
		return clientSleepTime;
	}
	
	/**
	 * netty客户端长连向服务端发送心跳时间，当前注册中心客户端时间间隔必需小于其他注册中心serverIdleStateTime
	 */
	private int clientIdleStateTime = 1000*10;
	
	/**
	 * 设置netty客户端长连向服务端发送心跳时间，当前注册中心客户端时间间隔必需小于其他注册中心serverIdleStateTime
	 * @param clientIdleStateTime
	 */
	public void setClientIdleStateTime(int clientIdleStateTime){
		this.clientIdleStateTime = clientIdleStateTime;
	}
	
	/**
	 * netty客户端长连向服务端发送心跳时间，当前注册中心客户端时间间隔必需小于其他注册中心serverIdleStateTime
	 * @return
	 */
	public int getClientIdleStateTime() {
		return clientIdleStateTime;
	}

	/**
	 * 验证签名允许的最大时间间隔
	 */
	private long timeInterval = 60000;
	
	/**
	 * 设置验证签名允许的最大时间间隔
	 * @param timeInterval
	 */
	public void setTimeInterval(long timeInterval){
		this.timeInterval = timeInterval;
	}
	
	/**
	 * 验证签名允许的最大时间间隔
	 * @return
	 */
	public long getTimeInterval(){
		return timeInterval;
	}

	/**
	 * 密码
	 */
	private String secretKey = null;
	
	/**
	 * 设置密码
	 * @param secretKey
	 */
	public void setSecretKey(String secretKey){
		this.secretKey = secretKey;
	}
	
	/**
	 * 密码
	 * @return
	 */
	public String getSecretKey(){
		return secretKey;
	}
}
