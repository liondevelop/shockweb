package com.shockweb.configcenter.data;



/**
 * 带group和name的请求
 * 
 * @author 彭明华
 * 2018年3月20日 创建
 */
public class ReqName implements IReq{
	
	/**
	 * 分组
	 */
	private String group = null;
	
	/**
	 * 配置分组
	 * @param group
	 */
	public void setGroup(String group){
		this.group = group;
	}
	
	/**
	 * 分组
	 * @return
	 */
	public String getGroup(){
		return group;
	}
	
	/**
	 * 关键字
	 */
	private String name = null;
	
	/**
	 * 配置关键字
	 * @param name
	 */
	public void setName(String name){
		this.name = name;
	}
	
	/**
	 * 关键字
	 * @return
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * 签名值
	 */
	private String sign = null;
	
	/**
	 * 配置签名值
	 * @param sign
	 */
	public void setSign(String sign){
		this.sign = sign;
	}
	
	/**
	 * 签名值
	 * @return
	 */
	public String getSign(){
		return sign;
	}
	
	/**
	 * 时间
	 */
	private long time = System.currentTimeMillis();
	
	/**
	 * 配置时间
	 * @param name
	 */
	public void setTime(long time){
		this.time = time;
	}
	
	/**
	 * 时间
	 * @return
	 */
	public long getTime(){
		return time;
	}
    
	/**
	 * @see Object#toString()
	 */
	public String toString(){
		StringBuilder sb = new StringBuilder("group:");
		sb.append(group);
		sb.append(",name:");
		sb.append(name);
		sb.append(",time:");
		sb.append(time);
		return sb.toString();
	}
	
}
