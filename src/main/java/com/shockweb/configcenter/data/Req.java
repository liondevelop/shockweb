package com.shockweb.configcenter.data;



/**
 * 空亲求对象
 * 
 * @author 彭明华
 * 2018年3月20日 创建
 */
public class Req implements IReq{
	
    
	/**
	 * 签名值
	 */
	private String sign = null;
	
	/**
	 * 配置签名值
	 * @param sign
	 */
	public void setSign(String sign){
		this.sign = sign;
	}
	
	/**
	 * 签名值
	 * @return
	 */
	public String getSign(){
		return sign;
	}
	
	/**
	 * 时间
	 */
	private long time = System.currentTimeMillis();
	
	/**
	 * 配置时间
	 * @param name
	 */
	public void setTime(long time){
		this.time = time;
	}
	
	/**
	 * 时间
	 * @return
	 */
	public long getTime(){
		return time;
	}
    
	/**
	 * @see Object#toString()
	 */
	public String toString(){
		StringBuilder sb = new StringBuilder("time:");
		sb.append(time);
		return sb.toString();
	}
	
}
