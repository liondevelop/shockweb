package com.shockweb.configcenter.data;



/**
 * 请求接口
 * 
 * @author 彭明华
 * 2018年3月20日 创建
 */
public interface IReq{
	
  
	
	/**
	 * 签名值
	 * @return
	 */
	public String getSign();

	/**
	 * 时间
	 * @return
	 */
	public long getTime();
    
	/**
	 * @see Object#toString()
	 */
	public String toString();
	
}
