package com.shockweb.utils;


/**
 * 线程抽象类
 * @author pengminghua
 *
 */
public abstract class AbstractSyncThread implements Runnable{


	
	/**
	 * 启动的同步线程
	 */
	private Thread thread = null;
	
	/**
	 * 启动服务
	 * @param servers
	 */
	protected final synchronized void start(){
		synchronized(this) {
			thread = new Thread(this);
			thread.start();
		}
	}
	
	/**
	 * 公共延迟方法
	 */
	protected final static void timeDelay(int time) {
		try {
			long t = time - System.currentTimeMillis() % time;
			if (t > 0) {
				Thread.sleep(t);
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
    
	/**
	 * 停止服务
	 */
	protected final synchronized void stop() {
		synchronized(this) {
			thread.interrupt();
			while(thread.isAlive()){
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
				}
			}
		}
	}
}
