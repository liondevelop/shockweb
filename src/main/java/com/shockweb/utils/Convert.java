package com.shockweb.utils;

import com.shockweb.common.serializable.binary.BinaryReader;
import com.shockweb.common.serializable.binary.BinaryWriter;
/**
 * 对象转换类
 * 
 * @author 彭明华
 * 2017年12月13日 创建
 */

public class Convert {

	/**
	 * 将二进制数据转换成对象
	 * @param data
	 * @param offset
	 * @param len
	 * @param clazz
	 * @return
	 * @throws Exception
	 */
	public static Object convertToObject(byte[] data,int offset,int len,Class<?> clazz) throws Exception{
		return new BinaryReader().readBinary(data,offset);
	}
	/**
	 * 将成对象转换二进制数据
	 * @param object
	 * @return
	 * @throws Exception
	 */
	public static byte[] convertToBytes(Object object) throws Exception{
		return new BinaryWriter().toBinarys(object);
	}
}
