package com.shockweb.register;

import java.io.File;
import java.io.IOException;
import java.nio.channels.Selector;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.MultithreadEventLoopGroup;
import io.netty.channel.ServerChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.timeout.IdleStateHandler;
import com.shockweb.common.utils.FileTools;
import com.shockweb.common.utils.PropertyFile;
import com.shockweb.client.exception.ClientException;
import com.shockweb.client.impl.ConfigCenterClient;
import com.shockweb.common.log.LogManager;
import com.shockweb.register.config.RegisterConfig;
import com.shockweb.service.ServiceServer;
import com.shockweb.service.exception.ServerException;

/**
 * 注册服务器
 * 
 * @author 彭明华
 * 2018年1月3日 创建
 */
public class RegisterServer implements Runnable{

    /**
     * 当前服务器的url
     */
	private RegisterConfig config = null;
    
	/**
	 * 当前注册中心实例
	 */
    private static RegisterServer server = new RegisterServer();
    
    /**
     * {@link Bootstrap} sub-class which allows easy bootstrap of {@link ServerChannel}
     *
     */
    private volatile ServerBootstrap b;
    
    /**
     * 获取当前注册中心的配置
     * @return
     */
	public RegisterConfig getConfig() {
		return config;
	}
	
	/**
	 * 默认的启动方法
	 * 指定配置文件的路径
	 * @param args
	 * @throws RegisterServerException
	 */
	public static void main(String[] args) throws RegisterServerException{
		if(args!=null && args.length==1){
			startCluster(args[0]);
		}else if(args!=null && args.length==2){
			startConfigCenter(args[0],args[1]);
		}else{
			startCluster(FileTools.getClassPath());
		}
	}
	
	/**
	 * 通过配置中心启动
	 * @param urls
	 * @param group
	 * @throws RegisterServerException
	 */
    public static void startConfigCenter(String urls,String group) throws RegisterServerException {
       	try{
	    	server = new RegisterServer();
	    	server.config = new RegisterConfig();
	    	if(urls!=null){
	    		server.config.setConfigCenterUrls(urls.replaceAll(" ", ""));
	    	}
	    	if(group!=null){
	    		server.config.setConfigCenterGroup(group.replaceAll(" ", ""));
	    	}
	    	if(server.config.getConfigCenterUrls()!=null){
	    		configCenterSetConfig(server.config.getConfigCenterUrls(),server.config.getConfigCenterGroup());
	    	}
	    	server.startUp();
    	}catch(Exception e){
    		throw new RegisterServerException("启动服务",e);
    	}
    }
    
	/**
	 * 启动注册中心方法,带配置文件路径的集群方式启动
	 * @param propFilePath
	 * @throws IOException
	 */
    public static void startCluster(String filePath) throws RegisterServerException {
    	try{
    		if(filePath.startsWith("classpath:")){
    			ClassLoader classLoader = null;
    			if(Thread.currentThread()!=null){
    				classLoader = Thread.currentThread().getContextClassLoader();
    			}else{
    				classLoader = ServiceServer.class.getClassLoader();
    			}
    			filePath = classLoader.getResource(filePath.substring("classpath:".length())).getPath();
    		}
	    	String file = null;
	    	if(new File(filePath).isFile()){
	    		file = filePath;
	    	}else if(new File(filePath).isDirectory()){
	    		file = FileTools.getFullPathFileName(filePath,"config.properties");
	    	}else{
	    		throw new ServerException("配置文件路径" + filePath + "非法");
	    	}
	    	server = new RegisterServer();
	    	server.config = new RegisterConfig();
	    	Map<String,String> data = PropertyFile.read(file);
	    	if(data==null){
	    		throw new RegisterServerException("配置文件" + file + "内容为空");
	    	}
	    	if(data.get("register.configCenterUrls")!=null){
	    		server.config.setConfigCenterUrls(data.get("register.configCenterUrls").replaceAll(" ", ""));
	    	}
	    	if(data.get("register.configCenterGroup")!=null){
	    		server.config.setConfigCenterGroup(data.get("register.configCenterGroup").replaceAll(" ", ""));
	    	}

	    	if(data.get("register.hostUrl")!=null){
	    		server.config.setHostUrl(data.get("register.hostUrl").replaceAll(" ", ""));
	    	}
	    	if(data.get("register.registerServerUrls")!=null){
	    		server.config.setRegisterServerUrls(data.get("register.registerServerUrls").replaceAll(" ", ""));
	    	}
	    	if(data.get("register.activeTime")!=null){
	    		server.config.setActiveTime(Integer.parseInt(data.get("register.activeTime").trim()));
	    	}
	    	if(data.get("register.syncThreadSleepTime")!=null){
	    		server.config.setSyncThreadSleepTime(Integer.parseInt(data.get("register.syncThreadSleepTime").trim()));
	    	}
	    	if(data.get("register.serverIdleStateTime")!=null){
	    		server.config.setServerIdleStateTime(Integer.parseInt(data.get("register.serverIdleStateTime").trim()));
	    	}
	    	if(data.get("register.clientConnectTimeOut")!=null){
	    		server.config.setClientConnectTimeOut(Integer.parseInt(data.get("register.clientConnectTimeOut").trim()));
	    	}
	    	if(data.get("register.clientTimeOut")!=null){
	    		server.config.setClientTimeOut(Integer.parseInt(data.get("register.clientTimeOut").trim()));
	    	}
	    	if(data.get("register.clientSleepTime")!=null){
	    		server.config.setClientSleepTime(Integer.parseInt(data.get("register.clientSleepTime")));
	    	}
	    	if(data.get("register.clientIdleStateTime")!=null){
	    		server.config.setClientIdleStateTime(Integer.parseInt(data.get("register.clientIdleStateTime").trim()));
	    	}
	    	if(server.config.getConfigCenterUrls()!=null){
	    		configCenterSetConfig(server.config.getConfigCenterUrls(),server.config.getConfigCenterGroup());
	    	}
	    	server.startUp();
    	}catch(Exception e){
    		throw new RegisterServerException("启动服务",e);
    	}
    }
    
    /**
     * 通过配置中心设置配置
     * @param urls
     * @param group
     */
    private static void configCenterSetConfig(String urls,String group)throws ClientException{
    	if(urls!=null){
    		ConfigCenterClient client = null;
    		try{
	    		client = ConfigCenterClient.getConfigCenterClient(server.config.getConfigCenterUrls());
	        	if(group==null){
	        		group = "register";
	        	}
	        	String hostUrl = client.getConfig(group, "hostUrl");
		    	if(hostUrl!=null){
		    		server.config.setHostUrl(hostUrl.replaceAll(" ", ""));
		    	}
		    	String registerServerUrls = client.getConfig(group, "registerServerUrls");
		    	if(registerServerUrls!=null){
		    		server.config.setRegisterServerUrls(registerServerUrls.replaceAll(" ", ""));
		    	}
		    	String activeTime = client.getConfig(group, "activeTime");
		    	if(activeTime!=null){
		    		server.config.setActiveTime(Integer.parseInt(activeTime.trim()));
		    	}
		    	String syncThreadSleepTime = client.getConfig(group, "syncThreadSleepTime");
		    	if(syncThreadSleepTime!=null){
		    		server.config.setSyncThreadSleepTime(Integer.parseInt(syncThreadSleepTime.trim()));
		    	}
		    	String serverIdleStateTime = client.getConfig(group, "serverIdleStateTime");
		    	if(serverIdleStateTime!=null){
		    		server.config.setServerIdleStateTime(Integer.parseInt(serverIdleStateTime.trim()));
		    	}
		    	String clientConnectTimeOut = client.getConfig(group, "clientConnectTimeOut");
		    	if(clientConnectTimeOut!=null){
		    		server.config.setClientConnectTimeOut(Integer.parseInt(clientConnectTimeOut.trim()));
		    	}
		    	String clientTimeOut = client.getConfig(group, "clientTimeOut");
		    	if(clientTimeOut!=null){
		    		server.config.setClientTimeOut(Integer.parseInt(clientTimeOut.trim()));
		    	}
		    	String clientSleepTime = client.getConfig(group, "clientSleepTime");
		    	if(clientSleepTime!=null){
		    		server.config.setClientSleepTime(Integer.parseInt(clientSleepTime));
		    	}
		    	String clientIdleStateTime = client.getConfig(group, "clientIdleStateTime");
		    	if(clientIdleStateTime!=null){
		    		server.config.setClientIdleStateTime(Integer.parseInt(clientIdleStateTime.trim()));
		    	}
	    	}finally{
	    		if(client!=null){
	    			client.close();
	    		}
	    	}
    	}
    }
    
	/**
	 * 集群方式启动注册中心方法，带基本参数的集群方式启动
	 * @param hostUrl 当前注册中心url
	 * @param registerServerUrls 其他注册中心url，多个注册中心用逗号分隔
	 */
    public static void startCluster(String hostUrl,String registerServerUrls){
    	server.config = new RegisterConfig();
    	if(hostUrl!=null){
    		server.config.setHostUrl(hostUrl.replaceAll(" ", ""));
    	}
    	if(registerServerUrls!=null){
    		server.config.setRegisterServerUrls(registerServerUrls.replaceAll(" ", ""));
    	}
    	server.startUp();
    }
    
	/**
	 * 集群方式启动注册中心方法，完整的配置参数
	 * @param config
	 */
    public static void startCluster(RegisterConfig config){
    	server.config = config;
    	server.startUp();
    }
    
	/**
	 * 启动注册中心方法,默认网卡地址和3000端口
	 */
    public static void start() {
    	server.config = new RegisterConfig();
    	server.config.setHostUrl("0.0.0.0:3000");
    	server.startUp();
    }
    
    /**
     * 公用的启动服务方法
     */
    private synchronized void startUp(){
    	LogManager.infoLog(this.getClass(),"server=" + config.getHostUrl());
    	Thread thread = new Thread(this);
    	thread.start();
    }
    
    /**
     * 启动注册中心方法，完整的配置参数参数
     * @param config
     */
    public static void start(RegisterConfig config) {
    	server.config = config;
    	Thread thread = new Thread(server);
    	thread.start();
    }
    
    /**
     * 启动注册中心方法，只定地址端口hostUrl
     * @param hostUrl
     */
    public static void start(String hostUrl) {
    	server.config = new RegisterConfig();
    	if(hostUrl!=null){
    		server.config.setHostUrl(hostUrl.replace(" ", ""));
    	}
    	Thread thread = new Thread(server);
    	thread.start();
    }
    
	/**
	 * {@link MultithreadEventLoopGroup} implementations which is used for NIO {@link Selector} based {@link Channel}s.
	 */
    private volatile EventLoopGroup workerGroup;
    
    /**
     * {@link EventLoopGroup}
     */
    EventLoopGroup bossGroup;
    /**
     * 线程池
     */
    private volatile ExecutorService cachedThreadPool;
    
    /**
     * 注册中心同步线程
     */
    private SyncThread syncThread = null;
    /**
     * 异步启动注册中心方法
     */
    public void run() {
        // 一个线程组用于处理服务器接收客户端的连接
        // 另一个线程组用于处理SocketChannel的网络读写，用于网络事件的处理
        // EventLoopGroup就是用来管理调度他们的，注册Channel，管理他们的生命周期
        bossGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();
        cachedThreadPool = Executors.newCachedThreadPool();
        syncThread = new SyncThread(config);
        try {
            // ServerBootstrap用于启动ServerChannel的，是服务端的工具类，Bootstrap是用于启动Channel，
            b = new ServerBootstrap();
            final EventLoopGroup workerGroup = this.workerGroup;
            b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast("frameDecoder", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
                            ch.pipeline().addLast("frameEncoder", new LengthFieldPrepender(4));
                            //当有操作操作超出指定空闲秒数时，便会触发UserEventTriggered事件
                            ch.pipeline().addLast(new IdleStateHandler(config.getServerIdleStateTime(), 0, 0, TimeUnit.MILLISECONDS));
                            ch.pipeline().addLast(workerGroup, new RegisterServerNettyHandler(cachedThreadPool,syncThread));// 业务处理
                            //ch.pipeline().addLast(new LoggingHandler(LogLevel.INFO));
                        }
                    }).option(ChannelOption.SO_BACKLOG, 10000);// 配置TCP参数
            b.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
            b.childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
            //通过NoDelay禁用Nagle,使消息立即发出去，不用等待到一定的数据量才发出去
            b.option(ChannelOption.TCP_NODELAY, true);
            b.childOption(ChannelOption.SO_KEEPALIVE, false); // 不保持常连接状态
            b.option(ChannelOption.SO_TIMEOUT, 5000);
            b.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000);
            doBind();
        } catch (Exception e) {
            LogManager.errorLog(this.getClass(),"启动注册服务器失败 Server=" + config.getHostUrl(),e);
        } finally {

            // 优雅退出 释放线程池资源
        	if(workerGroup!=null) {
        		workerGroup.shutdownGracefully();
        	}
        	if(bossGroup!=null) {
        		bossGroup.shutdownGracefully();
        	}
            if(cachedThreadPool!=null) {
            	cachedThreadPool.shutdown();
            }
            if(syncThread!=null) {
            	syncThread.close();
            }
            LogManager.infoLog(this.getClass(), "注册服务器停止 Server=" + config.getHostUrl());
        }
    }

    /**
     * 启动服务监听
     */
    protected void doBind() {
        ChannelFuture f;
        try {
        	if(config.getHostUrl()==null || config.getHostUrl().trim().equals("")) {
        		config.setHostUrl("0.0.0.0:3000");
        	}
        	String[] tmp = config.getHostUrl().split(":");
        	int port = 3000;
        	String host = tmp[0];
        	if(tmp.length>1){
        		port = Integer.parseInt(tmp[1]);
        	}
            f = b.bind(host, port).sync();
            f.addListener(new ChannelFutureListener() {
                public void operationComplete(final ChannelFuture f)throws Exception {
                    if (f.isSuccess()) {
                        LogManager.infoLog(this.getClass(),"注册服务器启动成功  Server=" + config.getHostUrl());
                    }else{
                    	LogManager.infoLog(this.getClass(), "注册服务器启动失败 Server=" + f.cause().getMessage());
                    }
                }
            });
            f.channel().closeFuture().sync();
        } catch (Exception e) {
        	LogManager.errorLog(this.getClass(),"启动doBind失败>>> Server=" + config.getHostUrl(),e);
        }
    }

	/**
	 * 关闭注册中心
	 */
    public synchronized static void stop() {
        try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
    	if(server.workerGroup!=null){
    		server.workerGroup.shutdownGracefully();
    	}
    	if(server.bossGroup!=null){
    		server.bossGroup.shutdownGracefully();
    	}
	}
    
}
