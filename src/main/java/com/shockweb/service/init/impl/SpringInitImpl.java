package com.shockweb.service.init.impl;

import java.io.File;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.shockweb.common.utils.FileTools;
import com.shockweb.service.config.ServiceConfig;
import com.shockweb.service.exception.ServerException;
import com.shockweb.service.init.Initialization;
import com.shockweb.service.ioc.IocManager;
import com.shockweb.service.ioc.impl.SpringIocInvokeImpl;

/**
 * Spring的初始化类
 * 
 * @author 彭明华
 * 2018年1月19日 创建
 */
public class SpringInitImpl implements Initialization{

	
	/**
	 * 初始化方法
	 */
	public void init(String path,ServiceConfig config)throws ServerException{
		String file = null;
		try{
			ApplicationContext ctx = null;
			if(path.startsWith("class:")){
				file = path.substring("class:".length());
				ctx = new ClassPathXmlApplicationContext(file);
			}else{
		    	if(new File(path).isFile()){
		    		file = path;
		    	}else if(new File(path).isDirectory()){
		    		file = FileTools.getFullPathFileName(path,"applicationContext.xml");
		    	}else{
		    		throw new ServerException("配置文件路径" + path + "非法");
		    	}
		    	ctx = new FileSystemXmlApplicationContext(file);
			}
			IocManager.add(new SpringIocInvokeImpl(ctx));
		}catch(Exception e){
			throw new ServerException("加载Spring'" + file + "'配置文件出错",e);
		}
	}
}
