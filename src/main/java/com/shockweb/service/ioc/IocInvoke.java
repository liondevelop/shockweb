package com.shockweb.service.ioc;

import com.shockweb.bridge.ServiceRequest;
import com.shockweb.service.exception.ServerException;

/**
 * 注入的初始化接口
 * 
 * @author 彭明华
 * 2018年1月19日 创建
 */
public interface IocInvoke{

	/**
	 * 获取服务的对象实例
	 * @param serviceName
	 * @param req
	 * @return
	 * @throws Exception
	 */
	public Object invoke(String serviceName,ServiceRequest req)throws ServerException;

	/**
	 * 检测该服务是否可以执行
	 * @param serviceName
	 * @return
	 */
	public boolean ableInvoke(String serviceName);
}
