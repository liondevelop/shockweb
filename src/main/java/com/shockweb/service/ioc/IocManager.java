package com.shockweb.service.ioc;

import java.util.ArrayList;
import java.util.List;

import com.shockweb.bridge.ServiceRequest;
import com.shockweb.service.exception.ServiceException;
import com.shockweb.service.exception.ServerException;

/**
 * ioc管理器
 * 
 * @author 彭明华
 * 2018年1月19日 创建
 */
public class IocManager {

	/**
	 * IOC容器管理类
	 */
	private static List<IocInvoke> invokes = new ArrayList<IocInvoke>();
	
	/**
	 * 添加IOC容器管理类
	 * @param invoke
	 */
	public static void add(IocInvoke invoke){
		invokes.add(invoke);
	}
	
	/**
	 * 执行服务的方法
	 * @param serviceName
	 * @param req
	 * @return
	 * @throws ServerException
	 */
	public static Object invoke(String serviceName,ServiceRequest req)throws ServerException{
		if(invokes!=null){
			for(IocInvoke invoke:invokes){
				if(invoke.ableInvoke(req.getService())){
					try{
						return invoke.invoke(serviceName,req);
					}catch(ServiceException e){
						throw e;
					}catch(Exception e){
						new ServerException("执行服务失败",e);
					}
				}
			}
			throw new ServerException("找不到serviceName '" + req.getService() + "'对应的服务");
		}else{
			throw new ServerException("未定义IOC容器，无法执行服务");
		}
	}
}
