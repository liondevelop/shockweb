package com.shockweb.service.ioc.impl.spring;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;

/**
 * 自定义spring扫描类路径
 * 
 * @author 彭明华
 * 2018年1月26日 创建
 */
public class ServiceBeanScannerConfigurer implements  BeanFactoryPostProcessor, ApplicationContextAware {
	/**
	 * spring上下文
	 */
	private ApplicationContext applicationContext;

	/**
	 * @see ApplicationContextAware#setApplicationContext(ApplicationContext)
	 */
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
	
	/**
	 * 扫描的包路径
	 */
	private String basePackage;
	
	/**
	 * 设置扫描的包路劲
	 * @param basePackage
	 */
	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}
	

	/**
	 * @see BeanFactoryPostProcessor#postProcessBeanFactory(ConfigurableListableBeanFactory)
	 */
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		Scanner scanner = new Scanner((BeanDefinitionRegistry) beanFactory);
		scanner.setResourceLoader(this.applicationContext);
		scanner.scan(basePackage.split(","));
	}
}