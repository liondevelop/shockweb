package com.shockweb.service.ioc.defaultconfig;

/**
 * 定义Service的类
 * 
 * @author 彭明华
 * 2018年1月17日 创建
 */
public class Service {

	/**
	 * 类对应的class
	 */
	private String clazz = null;
	
	/**
	 * 设置类对应的class
	 * @param clazz
	 */
	public void setClass(String clazz){
		this.clazz = clazz;
	}
	
	/**
	 * 类对应的class
	 */
	public String getClazz(){
		return clazz;
	}
	
	
	/**
	 * 执行前类name
	 */
	private String before = null;
	
	/**
	 * 设置执行前类name
	 * @param before
	 */
	public void setBefore(String before){
		this.before = before;
	}
	
	/**
	 * 执行前类name
	 */
	public String getBefore(){
		return before;
	}
	
	/**
	 * 执行后类after
	 */
	private String returning = null;
	
	/**
	 * 设置执行后类returning
	 * @param returning
	 */
	public void setReturning(String returning){
		this.returning = returning;
	}
	
	/**
	 * 执行后类after
	 */
	public String getReturning(){
		return returning;
	}
	
	/**
	 * 执行异常类name
	 */
	private String throwing = null;
	
	/**
	 * 设置执行异常类name
	 * @param throwing
	 */
	public void setThrowing(String throwing){
		this.throwing = throwing;
	}
	
	/**
	 * 执行异常类name
	 */
	public String getThrowing(){
		return throwing;
	}
	
	/**
	 * 类的实例化类型
	 */
	private String scope = "singleton";
	
	/**
	 * 设置类的实例化类型
	 * @param scope
	 */
	public void setScope(String scope){
		this.scope = scope;
	}
	
	/**
	 * 类的实例化类型
	 */
	public String getScope(){
		return scope;
	}
	
	/**
	 * @see Object#toString()
	 */
	public String toString(){
		return "class:" + clazz + ",scope:" + scope + ",before:" + before + ",returning:" + returning + ",throwing:" + throwing;		
	}
}
