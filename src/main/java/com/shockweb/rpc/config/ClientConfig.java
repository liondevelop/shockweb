package com.shockweb.rpc.config;

/**
 * 远程微服务客户端调用配置
 * 
 * @author 彭明华
 * 2018年1月8日 创建
 */
public class ClientConfig {
	
	/**
	 * 注册服务器中心key
	 */
	private String registerCenterKey = null;
	/**
	 * 设置注册服务器中心key
	 * @param registerCenterKey
	 */
	public void setRegisterCenterKey(String registerCenterKey){
		this.registerCenterKey = registerCenterKey;
	}
	
	/**
	 * 注册服务器key
	 * @return
	 */
	public String getRegisterCenterKey(){
		return registerCenterKey;
	}

	/**
	 * 所有注册服务器集群的url，多个为逗号分隔
	 */
	private String registerServerUrls = null;
	
	/**
	 * 设置所有注册服务器集群的url，多个为逗号分隔
	 * @param registerServerUrls
	 */
	public void setRegisterServerUrls(String registerServerUrls){
		this.registerServerUrls = registerServerUrls;
	}
	
	/**
	 * 所有注册服务器集群的url，多个为逗号分隔
	 * @return
	 */
	public String getRegisterServerUrls(){
		return registerServerUrls;
	}


    /**
     * 客户端长连接活动的有效时间
     */
    private int activeTime = 30000;
    
    /**
     * 设置客户端长连接活动的有效时间
     * @param activeTime
     */
    public void setActiveTime(int activeTime){
    	this.activeTime = activeTime;
    }
    
	/**
	 * 客户端长连接活动的有效时间
	 * @return
	 */
	public int getActiveTime(){
		return activeTime;
	}
	
	/**
	 * 同步服务每个周期线程的等待时间
	 */
	public int syncThreadSleepTime = 1000;
	
	/**
	 * 设置同步服务每个周期线程的等待时间
	 * @param syncThreadSleepTime
	 */
	public void setSyncThreadSleepTime(int syncThreadSleepTime){
		this.syncThreadSleepTime = syncThreadSleepTime;
	}
	
	/**
	 * 同步服务每个周期线程的等待时间
	 * @return
	 */
	public int getSyncThreadSleepTime(){
		return syncThreadSleepTime;
	}
	
	/**
	 * 客户端建立连接的超时时间
	 */
	private int clientConnectTimeOut = 5000;
	
	/**
	 * 设置客户端建立连接的超时时间
	 * @param clientConnectTimeOut
	 */
	public void setClientConnectTimeOut(int clientConnectTimeOut){
		this.clientConnectTimeOut = clientConnectTimeOut;
	}
	
	/**
	 * 客户端建立连接的超时时间
	 * @return
	 */
	public int getClientConnectTimeOut() {
		return clientConnectTimeOut;
	}
	
	/**
	 * 客户端请求的超时时间
	 */
    private int clientTimeOut = 5000;
    
	/**
	 * 设置客户端请求的超时时间
	 * @param IdleStateTime
	 */
	public void setClientTimeOut(int clientTimeOut){
		this.clientTimeOut = clientTimeOut;
	}
	
	/**
	 * 客户端请求的超时时间
	 * @return
	 */
	public int getClientTimeOut() {
		return clientTimeOut;
	}
	

	/**
	 * 客户端等待时扫描时间结果
	 */
    private int clientSleepTime = 1;
    
	/**
	 * 设置客户端等待时扫描时间结果
	 * @param clientSleepTime
	 */
	public void setClientSleepTime(int clientSleepTime){
		this.clientSleepTime = clientSleepTime;
	}
	
	/**
	 * 客户端等待时扫描时间结果
	 * @return
	 */
	public int getClientSleepTime() {
		return clientSleepTime;
	}
	
	/**
	 * 长连接心跳发送时间
	 */
	private int clientIdleStateTime = 1000*10;
	
	/**
	 * 检测长连接心跳发送时间
	 * @param clientIdleStateTime
	 */
	public void setClientIdleStateTime(int clientIdleStateTime){
		this.clientIdleStateTime = clientIdleStateTime;
	}
	
	/**
	 * 长连接心跳发送时间
	 * @return
	 */
	public int getClientIdleStateTime() {
		return clientIdleStateTime;
	}

	/**
	 * 熔断统计调用失败的时间周期ms
	 */
    private long clientFuseCycleTime = 0;
    
	/**
	 * 设置熔断统计调用失败的时间周期ms
	 * @param clientFuseCycleTime
	 */
	public void setClientFuseCycleTime(long clientFuseCycleTime){
		this.clientFuseCycleTime = clientFuseCycleTime;
	}
	
	/**
	 * 熔断统计调用失败的时间周期ms
	 * @return
	 */
	public long getClientFuseCycleTime() {
		return clientFuseCycleTime;
	}
	
	 /**
	  * 熔断统计调用失败的时间周期被划分的数量
	  */
	private int clientFuseCycleTimeNum = 10;
	 
	/**
	 * 设置熔断统计调用失败的时间周期被划分的数量
	 * @param clientFuseCycleTimeNum
	 */
	public void setClientFuseCycleTimeNum(int clientFuseCycleTimeNum){
		this.clientFuseCycleTimeNum = clientFuseCycleTimeNum;
	}
	
	/**
	 * 熔断统计调用失败的时间周期被划分的数量
	 * @return
	 */
	public int getClientFuseCycleTimeNum() {
		return clientFuseCycleTimeNum;
	}
	 
	 /**
	  * 熔断发生后再次重试时间间隔ms
	  */
	private long clientFuseWaitInterval = 10*1000;
	 
	/**
	 * 设置熔断发生后再次重试时间间隔ms
	 * @param clientFuseWaitInterval
	 */
	public void setClientFuseWaitInterval(long clientFuseWaitInterval){
		this.clientFuseWaitInterval = clientFuseWaitInterval;
	}
	
	/**
	 * 熔断发生后再次重试时间间隔ms
	 * @return
	 */
	public long getClientFuseWaitInterval() {
		return clientFuseWaitInterval;
	}
	 
	 /**
	  * 熔断触发限额次数
	  */
	private long clientFuseErrorThreshold = -1;
	 
	/**
	 * 设置 熔断触发限额次数
	 * @param clientFuseErrorThreshold
	 */
	public void setClientFuseErrorThreshold(long clientFuseErrorThreshold){
		this.clientFuseErrorThreshold = clientFuseErrorThreshold;
	}
	
	/**
	 *  熔断触发限额次数
	 * @return
	 */
	public long getClientFuseErrorThreshold() {
		return clientFuseErrorThreshold;
	}
	 
	
	 /**
	  * 熔断触发限额错误百分比率
	  */
	private int clientFuseErrorPercentage = -1;

	/**
	 * 设置熔断触发限额错误百分比率
	 * @param clientFuseErrorPercentage
	 */
	public void setClientFuseErrorPercentage(int clientFuseErrorPercentage){
		this.clientFuseErrorPercentage = clientFuseErrorPercentage;
	}
	
	/**
	 *  熔断触发限额错误百分比率
	 * @return
	 */
	public int getClientFuseErrorPercentage() {
		return clientFuseErrorPercentage;
	}
	
	 /**
	  * 最大重试次数
	  */
	private int clientMaxAutoRetries = 0;

	/**
	 * 设置最大重试次数
	 * @param clientFuseErrorPercentage
	 */
	public void setClientMaxAutoRetries(int clientMaxAutoRetries){
		this.clientMaxAutoRetries = clientMaxAutoRetries;
	}
	
	/**
	 *  最大重试次数
	 * @return
	 */
	public int getClientMaxAutoRetries() {
		return clientMaxAutoRetries;
	}
}
