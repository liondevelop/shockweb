package com.shockweb.rpc.data;

import com.shockweb.client.impl.ServiceClient;

/**
 * 保存微服务的连接
 * 
 * @author 彭明华
 * 2018年1月11日 创建
 */
public class ServiceClientWapper {
	
	/**
	 * 构造方法
	 * @param client
	 */
	public ServiceClientWapper(ServiceClient client){
		timeMillis = System.currentTimeMillis();
		this.client = client;
	}
	
	/**
	 * 微服务连接
	 */
	private ServiceClient client = null;
	
	/**
	 * 检测连接激活状态
	 * @return
	 */
	public boolean isActive(){
		synchronized(client){
			return client.isActive();
		}
	}
	
	/**
	 * 关闭连接
	 */
	public void close(){
		synchronized(client){
			client.close();
		}
	}
	
	/**
	 * 获取微服务连接
	 * @return
	 */
	public ServiceClient getServiceClient(){
		timeMillis = System.currentTimeMillis();
		return client;
	}
	
	/**
	 * 使用时间
	 */
	private long timeMillis = 0;
	
	/**
	 * 获取使用时间
	 * @return
	 */
	public long getTimeMillis(){
		return timeMillis;
	}
	

}
