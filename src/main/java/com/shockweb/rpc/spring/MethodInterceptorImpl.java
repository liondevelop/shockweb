package com.shockweb.rpc.spring;

import java.lang.reflect.Method;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

/**
 * 方法拦截器
 * @see MethodInterceptor
 * 
 * @author 彭明华
 * 2018年1月26日 创建
 */
public class MethodInterceptorImpl implements MethodInterceptor {
	/**
	 * @see MethodInterceptor#intercept(Object, Method, Object[], MethodProxy)
	 */
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
	    return methodProxy.invokeSuper(o, objects);
	}


}