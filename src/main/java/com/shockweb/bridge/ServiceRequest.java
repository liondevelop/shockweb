package com.shockweb.bridge;

import com.shockweb.common.context.ContextParam;

/**
 * 调用微服务请求
 * 
 * @author 彭明华
 * 2018年1月8日 创建
 */
public class ServiceRequest {
	
	/**
	 * 空间名
	 */
	private String spaceName = null;
	
	/**
	 * 设置空间名
	 * @param spaceName
	 */
	public void setSpaceName(String spaceName){
		this.spaceName = spaceName;
	}
	
	/**
	 * 空间名
	 * @return
	 */
	public String getSpaceName(){
		return spaceName;
	}
	
	/**
	 * 微服务名
	 */
	private String service = null;
	
	/**
	 * 设置微服务名
	 * @param serviceName
	 */
	public void setService(String service){
		this.service = service;
	}
	
	/**
	 * 微服务名
	 * @return
	 */
	public String getService(){
		return service;
	}
	
	/**
	 * 调用微服务方法名
	 */
	private String method = null;
	
	/**
	 * 设置调用微服务方法名
	 * @param mothedName
	 */
	public void setMethod(String method){
		this.method = method;
	}
	
	/**
	 * 调用微服务方法名
	 * @return
	 */
	public String getMethod(){
		return method;
	}
	
	/**
	 * 上下文参数
	 */
	private ContextParam context = null;
	
	/**
	 * 设置上下文参数
	 * @param pararms
	 */
	public void setContext(ContextParam context){
		this.context = context;
	}
	
	/**
	 * 上下文参数
	 */
	public ContextParam getContext(){
		return context;
	}
	
	/**
	 * 微服务方法的参数类型
	 */
	private Class<?>[] parameterTypes = null;
	
	/**
	 * 设置微服务方法的参数类型
	 * @param parameterTypes
	 */
	public void setParameterTypes(Class<?>... parameterTypes){
		this.parameterTypes = parameterTypes;
	}
	
	/**
	 * 微服务方法的参数类型
	 */
	public Class<?>[] getParameterTypes(){
		return parameterTypes;
	}
	
	/**
	 * 微服务方法的参数
	 */
	private Object[] params = null;
	
	/**
	 * 设置微服务方法的参数
	 * @param pararms
	 */
	public void setParams(Object... params){
		this.params = params;
	}
	
	/**
	 * 微服务方法的参数
	 */
	public Object[] getParams(){
		return params;
	}
	
	/**
	 * @see Object#toString()
	 */
	public String toString(){
		return "service:" + service + ",method:" + method + ",context:" + context + ",params:" + params; 
	}
}
