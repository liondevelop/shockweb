package com.shockweb.bridge;

import com.shockweb.client.Client;

/**
 * 操作符，命令符
 * 
 * @author 彭明华
 * 2017年12月12日 创建
 */
public enum OperationDefine {

	
	ALIVE("alive",(byte)0),RES_SUCCESS("success",Client.SUCCESS),RES_ERROR("error",Client.ERROR),RES_SERVICES_ERROR("services error",Client.SERVICES_ERROR),
	/** 返回成功，带结果 **/ /**返回注册中心所有服务信息**/ /**返回注册中心服务状态 **/
	RES_RESULT("result",(byte)10),RES_ALL_SERVICES("all service",(byte)11),RES_SERVICES_STATUS("services status",(byte)12),
	/** 停止当前服务器 **/ /** 发布服务器 **/ /** 发布服务 **/
	REQ_STOP("stop",(byte)20),REQ_PUT_HOSTS("put hosts",(byte)21),REQ_PUT_SERVICES("put services",(byte)22),
	/** 同步服务 **/ /** 同步服务 **/ /** 清除服务 **/
	REQ_SYNC_HOSTS("sync hosts",(byte)23),REQ_SYNC_SERVICES("sync services",(byte)24),REQ_CLEAR_SERVICES("clear services",(byte)25),
	/** 同步服务 **/ /** 同步服务 **/ /** 清除服务 **/
	REQ_QUERY_SERVICES("query services",(byte)26),REQ_SYNC_CONFIG("sync config",(byte)27),REQ_PUT_CONFIG("put config",(byte)28),
	/** 获取配置一个配置 **/ /** 删除一个配置 **/ /** 清除所有配置 **/
	REQ_GET_CONFIG("get config",(byte)29),REQ_REMOVE_CONFIG("remove config",(byte)30),REQ_CLEAR_CONFIG("clear config",(byte)31),
	/** 获取所有的Group定义 **/ /** 获取Group下所有的Name定义 **/ /** 微服务 **/
	REQ_QUERY_CONFIGGROUP("query config group",(byte)32),REQ_QUERY_CONFIGNAME("query config name",(byte)33),REQ_SERVICE("service",(byte)101),
	/** 发送数据不做串行化 **/ /** 微服务的状态 **/
	REQ_SENDDATA("send data",(byte)102),REQ_SERVICES_STATUS("services status",(byte)103),
	/** 异步微服务 **/ /** 异步发送数据不做串行化  **/
	REQ_ASYNC_SENDDATA("async service",(byte)104),REQ_ASYNC_SERVICE("async send data",(byte)105);


	/**
	 * 名字
	 */
	String name;
	/**
	 * 值
	 */
	byte value;
	
	/**
	 * 构造方法
	 * @param name
	 * @param value
	 */
	private OperationDefine(String name,byte value){
		this.name = name;
		this.value = value;
	}

	/**
	 * 名字
	 * @return
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * 值
	 * @return
	 */
	public byte value(){
		return value;
	}
}
