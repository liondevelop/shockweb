package com.shockweb.bridge;

import java.util.HashMap;
import java.util.Map;

/**
 * 服务空间
 * 
 * @author 彭明华
 * 2017年12月12日 创建
 */
public class ServiceSpace {
	

	
	/**
	 * 服务空间名
	 */
	private String name = null;
	
	/**
	 * 设置服务空间名
	 * @return
	 */
	public void setName(String name){
		this.name = name;
	}
	
	/**
	 * 服务空间名
	 * @return
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * 服务定义
	 */
	private Map<String,Service> services = new HashMap<String,Service>();
	
	/**
	 * 获取serviceName对应的服务
	 * @param serviceName
	 * @return
	 */
	public Service getService(String serviceName){
		return services.get(serviceName);
	}
	
	/**
	 * 设置获取所有serviceName对应的服务
	 * @return
	 */
	public void setServices(Map<String,Service> services){
		this.services = services;
	}
	
	/**
	 * 获取所有serviceName对应的服务
	 * @return
	 */
	public Map<String,Service> getServices(){
		return services;
	}
	
	/**
	 * 设置serviceName对应的服务
	 * @param serviceName
	 * @param service
	 */
	public void putService(String serviceName,Service service){
		services.put(serviceName,service);
	}
	/**
	 * 删除serviceName对应的服务
	 * @param serviceName
	 */
	public void removeService(String serviceName){
		services.remove(serviceName);
	}
	
	/**
	 * 服务主机定义
	 */
	private Map<String,ServiceHost> serviceHosts = new HashMap<String,ServiceHost>();
	
	/**
	 * 设置获取host对应的主机
	 * @param host
	 * @return
	 */
	public void setServiceHosts(Map<String,ServiceHost> serviceHosts){
		this.serviceHosts = serviceHosts;
	}
	
	/**
	 * 获取host对应的主机
	 * @param host
	 * @return
	 */
	public ServiceHost getServiceHost(String host){
		return serviceHosts.get(host);
	}
	
	/**
	 * 获取所有host对应的主机
	 * @return
	 */
	public Map<String,ServiceHost> getServiceHosts(){
		return serviceHosts;
	}
	
	/**
	 * 设置host对应的主机
	 * @param host
	 * @param serviceHost
	 */
	public void putServiceHost(String host,ServiceHost serviceHost){
		serviceHosts.put(host,serviceHost);
	}
	/**
	 * 删除host对应的主机
	 * @param host
	 */
	public void removeServiceHost(String host){
		serviceHosts.remove(host);
	}

	/**
	 * @see Object#toString()
	 */
	public String toString(){
		StringBuilder sb = new StringBuilder("name:");
		sb.append(name).append(",services:").append(services);
		sb.append(",serviceHosts:").append(serviceHosts);
		return sb.toString();
	}
}
