package com.shockweb.bridge;

import java.io.UnsupportedEncodingException;


import com.shockweb.common.International;

/**
 * 数据协议解析工具类
 * 
 * @author 彭明华
 * 2017年12月14日 创建
 */
public class DataAgreement {
	/**
	 * 一个字节的长度
	 */
	public static final int BYTE_LENGTH = 1;
	/**
	 * uuid的长度
	 */
	public static final int UUID_LENGTH = 36;
	
	/**
	 * 解析操作符
	 * @param src
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static byte resolutionOperation(byte[] src) throws UnsupportedEncodingException{
        return src[0];
	}
	/**
	 * 解析uuid
	 * @param src
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String resolutionUUID(byte[] src) throws UnsupportedEncodingException{
        int offset = BYTE_LENGTH ;
        return new String(src, offset, UUID_LENGTH,International.CHARSET);
	}
	
	/**
	 * 获取错误信息
	 * @param src
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String resolutionError(byte[] src) throws UnsupportedEncodingException{
        int offset = BYTE_LENGTH + UUID_LENGTH;
        return new String(src, offset, src.length-offset-1,International.CHARSET);
	}
}
