package com.shockweb.proxy;


import java.io.File;
import java.io.IOException;
import java.nio.channels.Selector;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.MultithreadEventLoopGroup;
import io.netty.channel.ServerChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.timeout.IdleStateHandler;
import com.shockweb.common.utils.FileTools;
import com.shockweb.common.utils.PropertyFile;
import com.shockweb.client.exception.ClientException;
import com.shockweb.client.impl.ConfigCenterClient;
import com.shockweb.common.log.LogManager;
import com.shockweb.proxy.config.ProxyConfig;
import com.shockweb.register.RegisterServerException;
import com.shockweb.rpc.ClientManager;

/**
 * 代理服务器
 * 
 * @author 彭明华
 * 2018年3月30日 创建
 */
public class ProxyServer implements Runnable{

    /**
     * 当前服务器的url
     */
	private ProxyConfig config = null;

	/**
	 * 当前微中心实例
	 */
    private static ProxyServer server;
    
    /**
     * 客户端管理器
     */
    private ClientManager clientManager = null;
    /**
     * 返回当前服务器
     * @return
     */
    public static ProxyServer getServer(){
    	return server;
    }
    /**
     * {@link Bootstrap} sub-class which allows easy bootstrap of {@link ServerChannel}
     *
     */
    private volatile ServerBootstrap b;

    
    /**
     * 获取当前代理的配置
     * @return
     */
	public ProxyConfig getConfig() {
		return config;
	}
	
    /**
     * 返回客户端
     * @return
     */
    public ClientManager getClientManager(){
    	return clientManager;
    }
    
	/**
	 * 默认的启动函数，
	 * 指定配置文件的路径
	 * @param args
	 * @throws RegisterServerException
	 */
	public static void main(String[] args) throws ProxyException{
		if(args!=null && args.length==0){
			start(args[0]);
		}else if(args!=null && args.length==2){
			startConfigCenter(args[0],args[1]);
		}else{
			start(FileTools.getClassPath());
		}
	}
    
	/**
	 * 配置中心方式启动
	 * @param urls
	 * @param group
	 * @throws ProxyException
	 */
    public static void startConfigCenter(String urls,String group)throws ProxyException{
    	try{
	    	server = new ProxyServer();
	    	server.config = new ProxyConfig();
	    	if(urls!=null){
	    		server.config.setConfigCenterUrls(urls.replaceAll(" ", ""));
	    	}
	    	if(group!=null){
	    		server.config.setConfigCenterGroup(group.replaceAll(" ", ""));
	    	}
	    	if(server.config.getConfigCenterUrls()!=null){
	    		configCenterSetConfig(server.config.getConfigCenterUrls(),server.config.getConfigCenterGroup());
	    	}
	    	server.clientManager = new ClientManager(server.config);
	    	server.startUp();
    	}catch(ProxyException e){
    		throw e;
    	}catch(Exception e){
    		throw new ProxyException("启动代理服务器失败",e);
    	}
    }
	
	/**
	 * 启动代理方法,配置文件方式启动
	 * @param propFilePath
	 * @throws IOException
	 */
    public static void start(String filePath)throws ProxyException{
    	try{
    		if(filePath.startsWith("classpath:")){
    			ClassLoader classLoader = null;
    			if(Thread.currentThread()!=null){
    				classLoader = Thread.currentThread().getContextClassLoader();
    			}else{
    				classLoader = ProxyServer.class.getClassLoader();
    			}
    			filePath = classLoader.getResource(filePath.substring("classpath:".length())).getPath();
    		}
	    	String file = null;
	    	if(new File(filePath).isFile()){
	    		file = filePath;
	    	}else if(new File(filePath).isDirectory()){
	    		file = FileTools.getFullPathFileName(filePath,"config.properties");
	    	}else{
	    		throw new ProxyException("配置文件路径" + filePath + "非法");
	    	}
	    	server = new ProxyServer();
	    	server.config = new ProxyConfig();
	    	Map<String,String> data = PropertyFile.read(file);
	    	if(data==null){
	    		throw new ProxyException("配置文件" + file + "内容为空");
	    	}
	    	if(data.get("proxy.configCenterUrls")!=null){
	    		server.config.setConfigCenterUrls(data.get("proxy.configCenterUrls").replaceAll(" ", ""));
	    	}
	    	if(data.get("proxy.configCenterGroup")!=null){
	    		server.config.setConfigCenterGroup(data.get("proxy.configCenterGroup").replaceAll(" ", ""));
	    	}
	    	if(server.config.getConfigCenterUrls()!=null){
	    		configCenterSetConfig(server.config.getConfigCenterUrls(),server.config.getConfigCenterGroup());
	    	}
	    	if(data.get("proxy.hostUrl")!=null){
	    		server.config.setHostUrl(data.get("proxy.hostUrl").replaceAll(" ", ""));
	    	}
	    	if(data.get("proxy.serverIdleStateTime")!=null){
	    		server.config.setServerIdleStateTime(Integer.parseInt(data.get("proxy.serverIdleStateTime").trim()));
	    	}
	    	if(data.get("proxy.serviceTimeOut")!=null){
	    		server.config.setServiceTimeOut(Integer.parseInt(data.get("proxy.serviceTimeOut").trim()));
	    	}
	    	if(data.get("proxy.registerServerUrls")!=null){
	    		server.config.setRegisterServerUrls(data.get("proxy.registerServerUrls").replaceAll(" ", ""));
	    	}
	    	if(data.get("proxy.activeTime")!=null){
	    		server.config.setActiveTime(Integer.parseInt(data.get("proxy.activeTime").trim()));
	    	}
	    	if(data.get("proxy.syncThreadSleepTime")!=null){
	    		server.config.setSyncThreadSleepTime(Integer.parseInt(data.get("proxy.syncThreadSleepTime").trim()));
	    	}
	    	if(data.get("proxy.clientConnectTimeOut")!=null){
	    		server.config.setClientConnectTimeOut(Integer.parseInt(data.get("proxy.clientConnectTimeOut").trim()));
	    	}
	    	if(data.get("proxy.clientTimeOut")!=null){
	    		server.config.setClientTimeOut(Integer.parseInt(data.get("proxy.clientTimeOut").trim()));
	    	}
	    	if(data.get("proxy.clientSleepTime")!=null){
	    		server.config.setClientSleepTime(Integer.parseInt(data.get("proxy.clientSleepTime").trim()));
	    	}
	    	if(data.get("proxy.clientIdleStateTime")!=null){
	    		server.config.setClientIdleStateTime(Integer.parseInt(data.get("proxy.clientIdleStateTime").trim()));
	    	}
	    	if(data.get("proxy.clientFuseCycleTime")!=null){
	    		server.config.setClientFuseCycleTime(Long.parseLong(data.get("proxy.clientFuseCycleTime").trim()));
	    	}
	    	if(data.get("proxy.clientFuseCycleTimeNum")!=null){
	    		server.config.setClientFuseCycleTimeNum(Integer.parseInt(data.get("proxy.clientFuseCycleTimeNum").trim()));
	    	}
	    	if(data.get("proxy.clientFuseWaitInterval")!=null){
	    		server.config.setClientFuseWaitInterval(Long.parseLong(data.get("proxy.clientFuseWaitInterval").trim()));
	    	}
	    	if(data.get("proxy.clientFuseErrorThreshold")!=null){
	    		server.config.setClientFuseErrorThreshold(Long.parseLong(data.get("proxy.clientFuseErrorThreshold").trim()));
	    	}
	    	if(data.get("proxy.clientFuseErrorPercentage")!=null){
	    		server.config.setClientFuseErrorPercentage(Integer.parseInt(data.get("proxy.clientFuseErrorPercentage").trim()));
	    	}
	    	if(data.get("proxy.clientMaxAutoRetries")!=null){
	    		server.config.setClientMaxAutoRetries(Integer.parseInt(data.get("proxy.clientMaxAutoRetries").trim()));
	    	}
//	    	server.clientManager = new ClientManager(server.config);
	    	server.startUp();
    	}catch(ProxyException e){
    		throw e;
    	}catch(Exception e){
    		throw new ProxyException("启动微服务失败",e);
    	}
    }
    
    

    /**
     * 通过配置中心设置配置
     * @param urls
     * @param group
     */
    private static void configCenterSetConfig(String urls,String group)throws ClientException{
    	if(urls!=null){
    		ConfigCenterClient client = null;
    		try{
	    		client = ConfigCenterClient.getConfigCenterClient(server.config.getConfigCenterUrls());
	        	if(group==null){
	        		group = "proxy";
	        	}
		    	String hostUrl = client.getConfig(group, "hostUrl");
		    	if(hostUrl!=null){
		    		server.config.setHostUrl(hostUrl.replaceAll(" ",""));
		    	}
		    	String serverIdleStateTime = client.getConfig(group, "serverIdleStateTime");
		    	if(serverIdleStateTime!=null){
		    		server.config.setServerIdleStateTime(Integer.parseInt(serverIdleStateTime.trim()));
		    	}
		    	String serviceTimeOut = client.getConfig(group, "serviceTimeOut");
		    	if(serviceTimeOut!=null){
		    		server.config.setServiceTimeOut(Integer.parseInt(serviceTimeOut.trim()));
		    	}
		    	String registerServerUrls = client.getConfig(group, "registerServerUrls");
		    	if(registerServerUrls!=null){
		    		server.config.setRegisterServerUrls(registerServerUrls.replaceAll(" ", ""));
		    	}
		    	String activeTime = client.getConfig(group, "activeTime");
		    	if(activeTime!=null){
		    		server.config.setActiveTime(Integer.parseInt(activeTime.trim()));
		    	}
		    	String syncThreadSleepTime = client.getConfig(group, "syncThreadSleepTime");
		    	if(syncThreadSleepTime!=null){
		    		server.config.setSyncThreadSleepTime(Integer.parseInt(syncThreadSleepTime.trim()));
		    	}
		    	String clientConnectTimeOut = client.getConfig(group, "clientConnectTimeOut");
		    	if(clientConnectTimeOut!=null){
		    		server.config.setClientConnectTimeOut(Integer.parseInt(clientConnectTimeOut.trim()));
		    	}
		    	String clientTimeOut = client.getConfig(group, "clientTimeOut");
		    	if(clientTimeOut!=null){
		    		server.config.setClientTimeOut(Integer.parseInt(clientTimeOut.trim()));
		    	}
		    	String clientSleepTime = client.getConfig(group, "clientSleepTime");
		    	if(clientSleepTime!=null){
		    		server.config.setClientSleepTime(Integer.parseInt(clientSleepTime.trim()));
		    	}
		    	String clientIdleStateTime = client.getConfig(group, "clientIdleStateTime");
		    	if(clientIdleStateTime!=null){
		    		server.config.setClientIdleStateTime(Integer.parseInt(clientIdleStateTime.trim()));
		    	}
		    	String clientFuseCycleTime = client.getConfig(group, "clientFuseCycleTime");
		    	if(clientFuseCycleTime!=null){
		    		server.config.setClientFuseCycleTime(Long.parseLong(clientFuseCycleTime.trim()));
		    	}
		    	String clientFuseCycleTimeNum = client.getConfig(group, "clientFuseCycleTimeNum");
		    	if(clientFuseCycleTimeNum!=null){
		    		server.config.setClientFuseCycleTimeNum(Integer.parseInt(clientFuseCycleTimeNum.trim()));
		    	}
		    	String clientFuseWaitInterval = client.getConfig(group, "clientFuseWaitInterval");
		    	if(clientFuseWaitInterval!=null){
		    		server.config.setClientFuseWaitInterval(Long.parseLong(clientFuseWaitInterval.trim()));
		    	}
		    	String clientFuseErrorThreshold = client.getConfig(group, "clientFuseErrorThreshold");
		    	if(clientFuseErrorThreshold!=null){
		    		server.config.setClientFuseErrorThreshold(Long.parseLong(clientFuseErrorThreshold.trim()));
		    	}
		    	String clientFuseErrorPercentage = client.getConfig(group, "clientFuseErrorPercentage");
		    	if(clientFuseErrorPercentage!=null){
		    		server.config.setClientFuseErrorPercentage(Integer.parseInt(clientFuseErrorPercentage.trim()));
		    	}
		    	String clientMaxAutoRetries = client.getConfig(group, "clientMaxAutoRetries");
		    	if(clientMaxAutoRetries!=null){
		    		server.config.setClientMaxAutoRetries(Integer.parseInt(clientMaxAutoRetries.trim()));
		    	}
	    	}finally{
	    		if(client!=null){
	    			client.close();
	    		}
	    	}
    	}
    }
    

    
	/**
	 * 启动代理服务器方法,完整参数
	 * @param config
	 * @throws IOException 
	 */
    public static void start(ProxyConfig config)throws ProxyException{
    	server = new ProxyServer();
    	server.config = config;
    	server.startUp();
    }
    
	/**
	 * 启动注册中心方法,默认网卡地址和3000端口
	 */
    public static void start()throws ProxyException{
    	server = new ProxyServer();
    	server.config = new ProxyConfig();
    	server.config.setHostUrl("0.0.0.0:6300");
    	server.startUp();
    }
    
    /**
     * 启动服务
     */
    private synchronized void startUp()throws ProxyException{
    	LogManager.infoLog(this.getClass(),"server=" + config.getHostUrl());
    	Thread thread = new Thread(this);
    	thread.start();
    }
    

	/**
	 * {@link MultithreadEventLoopGroup} implementations which is used for NIO {@link Selector} based {@link Channel}s.
	 */
    private volatile EventLoopGroup workerGroup;
    
    /**
     * {@link EventLoopGroup} 
     */
    private volatile EventLoopGroup bossGroup;
    
    /**
     * 线程池
     */
    private volatile ExecutorService cachedThreadPool;
    /**
     * 异步启动方法
     */
    public void run() {
        // 一个线程组用于处理服务器接收客户端的连接
        // 另一个线程组用于处理SocketChannel的网络读写，用于网络事件的处理
        // EventLoopGroup就是用来管理调度他们的，微Channel，管理他们的生命周期
        bossGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();
        cachedThreadPool = Executors.newCachedThreadPool();
        server = this;
        try {
            // ServerBootstrap用于启动ServerChannel的，是服务端的工具类，Bootstrap是用于启动Channel，
            b = new ServerBootstrap();
            final EventLoopGroup workerGroup = this.workerGroup;
            b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast("frameDecoder", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
                            ch.pipeline().addLast("frameEncoder", new LengthFieldPrepender(4));
                            //当有操作操作超出指定空闲秒数时，便会触发UserEventTriggered事件
                            ch.pipeline().addLast(new IdleStateHandler(config.getServerIdleStateTime(), 0, 0, TimeUnit.MILLISECONDS));
                            ch.pipeline().addLast(workerGroup, new ProxyNettyHandler(config,cachedThreadPool));// 业务处理
                            //ch.pipeline().addLast(new LoggingHandler(LogLevel.INFO));
                        }
                    }).option(ChannelOption.SO_BACKLOG, 10000);// 配置TCP参数
            b.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
            b.childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
            //通过NoDelay禁用Nagle,使消息立即发出去，不用等待到一定的数据量才发出去
            b.option(ChannelOption.TCP_NODELAY, true);
            b.childOption(ChannelOption.SO_KEEPALIVE, false); // 不保持常连接状态
            b.option(ChannelOption.SO_TIMEOUT, 5000);
            b.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000);
            
            
            
            doBind();
        } catch (Exception e) {
            LogManager.errorLog(this.getClass(),"启动代理服务器器失败 Server=" + config.getHostUrl(),e);
        } finally {
            // 优雅退出 释放线程池资源
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
            cachedThreadPool.shutdown();
            LogManager.infoLog(this.getClass(), "代理服务器服务器停止 Server=" + config.getHostUrl());
        }
    }

    /**
     * 启动并绑定服务端口
     */
    protected void doBind() {
        ChannelFuture f;
        try {
        	if(config.getHostUrl()==null || config.getHostUrl().trim().equals("")) {
        		config.setHostUrl("0.0.0.0:6000");
        	}
        	String[] tmp = config.getHostUrl().split(":");
        	int port = 6000;
        	String host = tmp[0];
        	if(tmp.length>1){
        		port = Integer.parseInt(tmp[1]);
        	}
            f = b.bind(host, port).sync();
            f.addListener(new ChannelFutureListener() {
                public void operationComplete(final ChannelFuture f)throws Exception {
                    if (f.isSuccess()) {
                        LogManager.infoLog(this.getClass(),"代理服务器器启动成功  Server=" + config.getHostUrl());
                    }else{
                    	LogManager.infoLog(this.getClass(), "代理服务器器启动失败 Server=" + f.cause().getMessage());
                    }
                }
            });
            f.channel().closeFuture().sync();
        } catch (Exception e) {
        	LogManager.errorLog(this.getClass(),"启动代理服务器器失败 Server=" + config.getHostUrl(),e);
        }finally{
        	if(server.clientManager!=null){
        		server.clientManager.close();
        		server.clientManager = null;
        	}
        }
    }

	/**
	 * 停止服务
	 */
    public synchronized static void stop() {
    	try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
    	if(server.clientManager!=null){
    		server.clientManager.close();
    		server.clientManager = null;
    	}
    	if(server.workerGroup!=null){
    		server.workerGroup.shutdownGracefully();
    	}
    	if(server.bossGroup!=null){
    		server.bossGroup.shutdownGracefully();
    	}

        
	}
    
}
