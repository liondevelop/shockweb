package com.shockweb.client.impl;

import com.shockweb.common.International;
import com.shockweb.common.serializable.SerializableObject;
import com.shockweb.common.serializable.binary.BinaryReader;
import com.shockweb.rpc.ClientManager;
import com.shockweb.bridge.OperationDefine;
import com.shockweb.bridge.ServiceRequest;
import com.shockweb.bridge.ServiceResult;
import com.shockweb.client.Client;
import com.shockweb.client.exception.ClientException;
import com.shockweb.client.exception.ServerException;
import com.shockweb.client.exception.ServiceException;
import com.shockweb.client.exception.TimeoutException;
import com.shockweb.service.data.ServiceStatus;

/**
 * 微服务连接到微服务服务器客户端
 * 
 * @author 彭明华
 * 2018年1月2日 创建
 */
public class ServiceClient extends Client{
	
	private ClientManager manager = null;
	/**
	 * @see Client#Client()
	 */
	public ServiceClient(){
		super();
	}
	/**
	 * @see Client#Client(int, int, int, int)
	 * @param timeOut
	 * @param connectTimeOut
	 * @param sleepTime
	 * @param idleStateTime
	 */
    public ServiceClient(ClientManager manager,int timeOut,int connectTimeOut,int sleepTime,int idleStateTime){
    	super(timeOut,connectTimeOut,sleepTime,idleStateTime);
    	this.manager = manager;
    }
	
    /**
     * 查询微服务服务器所有服务
     * @return
     * @throws ClientException
     */
    public ServiceStatus queryServicesStatus() throws ClientException {
   		return (ServiceStatus)send(OperationDefine.REQ_SERVICES_STATUS,null,ServiceStatus.class);
    }
    
    
 
    /**
     * 调用远程服务，只发送byte[]数据和返回byte[]数据
     * @param serviceName 微服务名称
     * @param method 方法名
     * @param data 二进制数据
     * @return
     * @throws ClientException
     */
    public byte[] rpcService(String serviceName,String method,byte[] data) throws ClientException {
    	return rpcService(OperationDefine.REQ_SENDDATA,serviceName,method,data);
    }
    
    /**
     * 异步调用远程服务，只发送byte[]数据和返回byte[]数据
     * @param serviceName 微服务名称
     * @param method 方法名
     * @param data 二进制数据
     * @return
     * @throws ClientException
     */
    public void asyncRpcService(String serviceName,String method,byte[] data) throws ClientException {
    	rpcService(OperationDefine.REQ_ASYNC_SERVICE,serviceName,method,data);
    }
    
    /**
     * 基础调用远程服务，只发送byte[]数据和返回byte[]数据
     * @param operation
     * @param serviceName
     * @param method
     * @param data
     * @return
     * @throws ClientException
     */
    private byte[] rpcService(OperationDefine operation,String serviceName,String method,byte[] data) throws ClientException {
    	if(serviceName==null){
    		throw new ClientException("服务名不能为空");
    	}
    	try{
    		byte[] serviceNameBytes = serviceName.getBytes(International.CHARSET);
    		byte[] methodNameBytes = method.getBytes(International.CHARSET);
	    	if(data!=null){
	    		byte[] dataBytes = new byte[BinaryReader.LEN_INTEGER + serviceNameBytes.length + 
	    		                            BinaryReader.LEN_INTEGER + methodNameBytes.length + 
	    		                            data.length];
	    		SerializableObject.copyBytes(SerializableObject.intToBytes(serviceNameBytes.length),dataBytes, 0);
	    		SerializableObject.copyBytes(serviceNameBytes,dataBytes, BinaryReader.LEN_INTEGER);
	    		SerializableObject.copyBytes(SerializableObject.intToBytes(methodNameBytes.length),dataBytes, BinaryReader.LEN_INTEGER + serviceNameBytes.length);
	    		SerializableObject.copyBytes(methodNameBytes,dataBytes, BinaryReader.LEN_INTEGER + serviceNameBytes.length + BinaryReader.LEN_INTEGER);
	    		SerializableObject.copyBytes(data,dataBytes, BinaryReader.LEN_INTEGER + serviceNameBytes.length + BinaryReader.LEN_INTEGER + serviceNameBytes.length);
	    		return send(operation,dataBytes);
	    	}else{
	    		byte[] dataBytes = new byte[BinaryReader.LEN_INTEGER + serviceNameBytes.length + 
	    		                            BinaryReader.LEN_INTEGER + methodNameBytes.length];
	    		SerializableObject.copyBytes(SerializableObject.intToBytes(serviceNameBytes.length),dataBytes, 0);
	    		SerializableObject.copyBytes(serviceNameBytes,dataBytes, BinaryReader.LEN_INTEGER);
	    		SerializableObject.copyBytes(SerializableObject.intToBytes(methodNameBytes.length),dataBytes, BinaryReader.LEN_INTEGER + serviceNameBytes.length);
	    		SerializableObject.copyBytes(methodNameBytes,dataBytes, BinaryReader.LEN_INTEGER + serviceNameBytes.length + BinaryReader.LEN_INTEGER);
	    		return send(operation,dataBytes);
	    	}
	    } catch (ServiceException e) {
	    	throw e;
	    } catch (TimeoutException e) {
	    	manager.addInvokeErrorCount(this);
	    	throw e;
	    } catch (ServerException e) {
	    	manager.addInvokeErrorCount(this);
	    	throw e;
	    } catch (ClientException e) {
	        throw e;
	    } catch (Throwable e) {
	    	throw new ClientException("远程调用出错",e);
	    }finally{
	    	manager.addInvokeCount(this);
	    }
    }
    
    /**
     * 调用远程服务
     * @param request 完成服务请求
     * @return
     * @throws ClientException
     */
    public ServiceResult rpcService(ServiceRequest request) throws ClientException {
    	return rpcService(OperationDefine.REQ_SERVICE,request);
    	
    }
    
    /**
     * 异步调用远程服务
     * @param request 完成服务请求
     * @return
     * @throws ClientException
     */
    public void asyncRpcService(ServiceRequest request) throws ClientException {
    	rpcService(OperationDefine.REQ_ASYNC_SERVICE,request);
    }
    /**
     * 基础调用远程服务
     * @param operation
     * @param request
     * @return
     * @throws ClientException
     */
    private ServiceResult rpcService(OperationDefine operation,ServiceRequest request) throws ClientException {
    	if(request.getService()==null){
    		throw new ClientException("服务名不能为空");
    	}
    	try {
	   		return (ServiceResult)send(operation,request,ServiceResult.class);
	    } catch (ServiceException e) {
	    	throw e;
	    } catch (TimeoutException e) {
	    	manager.addInvokeErrorCount(this);
	    	throw e;
	    } catch (ServerException e) {
	    	manager.addInvokeErrorCount(this);
	    	throw e;
	    } catch (ClientException e) {
	        throw e;
	    } catch (Throwable e) {
	    	throw new ClientException("远程调用出错",e);
	    }finally{
	    	manager.addInvokeCount(this);
	    }
    }
    
}
