package com.shockweb.rpc;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.junit.Test;

import com.shockweb.common.context.ContextParam;
import com.shockweb.bridge.ServiceRequest;
import com.shockweb.rpc.config.ClientConfig;




/**
 * Unit test for simple App.
 */
public class RpcThreadTest implements Runnable{

	static ClientManager clientManager;

	static ServiceRequest request = new ServiceRequest();
	
	List<Thread> lists = new ArrayList<Thread>();
	
	@Test
    public  void test(){
		try{
			ClientConfig config = new ClientConfig();
			config.setRegisterServerUrls("127.0.0.1:3000");
			config.setRegisterCenterKey("238");
			RpcManager.addRegisterCenter(config);
			clientManager = RpcManager.getClientManager();
			System.out.println(clientManager.getRegisterClient().queryServices());
			
			request.setService("serviceTestNames");
			request.setContext(new ContextParam(UUID.randomUUID().toString()));
			byte[] arg = new byte[780];
			request.setParams(new Object[]{arg});
			for(int i=0;i<100;i++){
				clientManager.getClient("spaceName1", "serviceTestNames").rpcService(request);
			}
			for(int i=0;i<10;i++){
				lists.add(new Thread(new RpcThreadTest()));
				lists.get(i).start();
			}
			boolean stop = false;
			while(!stop){
				stop = true;
				for(Thread t:lists){
					if(t.isAlive()){
						stop = false;
						break;
					}
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println(clientManager.getRegisterClient().queryServices());
		}catch(Throwable e){
			e.printStackTrace();
		}finally{
			if(clientManager!=null){
				clientManager.close();
			}
		}
    }

	
	public void run(){
		long time = System.currentTimeMillis();
		System.out.println("start:" + time);
		for(int i=0;i<10000;i++){
			try{
				clientManager.getClient("spaceName1", "serviceTestNames").rpcService(request);
				if(i%1000==0){
					System.out.println("thread:" + Thread.currentThread() + i + "=" + (System.currentTimeMillis()-time)
							+ ",client:" + clientManager.getClient("spaceName1", "serviceTestNames").toString());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		System.out.println("end:" + Thread.currentThread() + (System.currentTimeMillis()-time));
	}

}
