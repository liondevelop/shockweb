package com.shockweb.rpc;

import java.util.UUID;
import org.junit.Test;
import com.shockweb.common.context.ContextParam;
import com.shockweb.bridge.ServiceRequest;
import com.shockweb.bridge.ServiceResult;
import com.shockweb.rpc.config.ClientConfig;


/**
 * Unit test for simple App.
 */
public class RpcTest{


    /**
     * @return the suite of tests being tested
     * @throws RpcException 
     */
	
	static ClientManager clientManager;
	
//	@Test
    public  void testRetries(){
		try{
			
			ClientConfig config = new ClientConfig();
			config.setRegisterServerUrls("127.0.0.1:3000");
			config.setRegisterCenterKey("238");
			config.setClientMaxAutoRetries(3);
			config.setClientTimeOut(200);
			RpcManager.addRegisterCenter(config);
			clientManager = RpcManager.getClientManager();
			System.out.println(clientManager.getRegisterClient().queryServices());
			ServiceRequest request = new ServiceRequest();
			request.setSpaceName("default");
			request.setService("TestService");
			request.setContext(new ContextParam(UUID.randomUUID().toString()));
			request.setParameterTypes(new Class[]{String.class,Integer.class});
			request.setParams(new Object[]{"123",new Integer(1)});
			request.setMethod("service");
			long success = 0;
			long error = 0;

			try{
				System.out.println(clientManager.getRegisterClient().queryServices());
				ServiceResult result = clientManager.rpcService(request);
//					System.out.println("result:" + result);
				Thread.sleep(10);
				success++;
			}catch(Exception e){
				error++;
				e.printStackTrace();
			}
			System.out.println("success:" + success + ",error:" + error);
		}catch(Throwable e){
			e.printStackTrace();
		}finally{
			if(clientManager!=null){
				clientManager.close();
			}
		}
		
	}
//	@Test
    public  void testFuse(){
		try{
			
			ClientConfig config = new ClientConfig();
			config.setRegisterServerUrls("127.0.0.1:3000");
			config.setRegisterCenterKey("238");
			
			config.setClientTimeOut(200);
			
			config.setClientFuseCycleTime(5000);
			config.setClientFuseCycleTimeNum(10);
			config.setClientFuseErrorThreshold(10);
			config.setClientFuseErrorPercentage(10);
//			config.setClientFuseWaitInterval(5000);
			RpcManager.addRegisterCenter(config);
			clientManager = RpcManager.getClientManager();
			System.out.println(clientManager.getRegisterClient().queryServices());
			ServiceRequest request = new ServiceRequest();
			request.setService("TestService");
			request.setContext(new ContextParam(UUID.randomUUID().toString()));
			request.setParameterTypes(new Class[]{String.class,Integer.class});
			request.setParams(new Object[]{"123",new Integer(1)});
			request.setMethod("service");
			long success = 0;
			long error = 0;
			for(int i=0;i<100;i++){
				try{
					ServiceResult result = clientManager.getClient("default", "TestService2").rpcService(request);
//					System.out.println("result:" + result);
					Thread.sleep(10);
					success++;
				}catch(Exception e){
					error++;
					e.printStackTrace();
				}
			}
			System.out.println("success:" + success + ",error:" + error);
		}catch(Throwable e){
			e.printStackTrace();
		}finally{
			if(clientManager!=null){
				clientManager.close();
			}
		}
		

    }
	

	@Test
    public  void test(){
		try{
			
			ClientConfig config = new ClientConfig();
			config.setRegisterServerUrls("127.0.0.1:3000");
			config.setRegisterCenterKey("238");
			RpcManager.addRegisterCenter(config);
			clientManager = RpcManager.getClientManager();
			System.out.println(clientManager.getRegisterClient().queryServices());
			
			ServiceRequest request = new ServiceRequest();
			request.setService("TestService");
			request.setContext(new ContextParam(UUID.randomUUID().toString()));
			request.setParameterTypes(new Class[]{String.class,Integer.class});
			request.setParams(new Object[]{"123",new Integer(1)});
			request.setMethod("service");
			ServiceResult result = clientManager.getClient("default", "TestService").rpcService(request);
			clientManager.getClient("default", "TestService3").rpcService("TestService3","service",new byte[]{0,1,2,3,4});
			System.out.println("result:" + result.toString());
			long time = System.currentTimeMillis();
			for(int n=0;n<3;n++) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						for(int i=0;i<100;i++){
							try{
//								result = clientManager.getClient("default", "TestService2").rpcService(request);
//								clientManager.rpcAllService("default", "TestService3","service",new byte[]{0,1,2,3,4});
								clientManager.getClient("default", "TestService").rpcService(request);
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
				}).start();
			}

			System.out.println("end:" + Thread.currentThread() + (System.currentTimeMillis()-time));
			System.out.println(clientManager.getRegisterClient().queryServices());
			Thread.sleep(10000);
		}catch(Throwable e){
			e.printStackTrace();
		}finally{
			if(clientManager!=null){
				clientManager.close();
			}
		}
		

    }


}
