package com.shockweb.rpc.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shockweb.rpc.spring.ScanClass11;

@Service
public class ScanClass1 {
	
//	@Autowired
//	ScanClass2 scanClass2;
	public void print() {
//		scanClass2.print();
//		System.out.println("scanClass1");
		scanInter11.print();
	}
	
	@Autowired
	ScanClass11 scanInter11;
}