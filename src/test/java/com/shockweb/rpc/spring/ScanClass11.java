package com.shockweb.rpc.spring;

import com.shockweb.rpc.spring.ShockWebRemote;

@ShockWebRemote(value="ScanClass11",spaceName="default")
public interface ScanClass11 extends ScanInter5{
	public void print();
	
	public byte[] test(byte[] data);
}