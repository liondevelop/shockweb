package com.shockweb.service.ioc.impl.spring;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.shockweb.common.context.ContextManager;
import com.shockweb.bridge.ServiceRequest;
import com.shockweb.rpc.RpcManager;

public class DefaultServiceRpcClient implements Runnable{
	
	ClassPathXmlApplicationContext ctx = null;
	
	public DefaultServiceRpcClient(ClassPathXmlApplicationContext ctx){
		this.ctx = ctx;
	}
	public void run(){
		ServiceRequest request = new ServiceRequest();
		request.setContext(ContextManager.getContextParam());
		request.setMethod("print");
		request.setService("ScanClass12");
		long time = System.currentTimeMillis();
		for(int i=0;i<10000;i++){
			try{
				RpcManager.getClientManager().getClient("default", "ScanClass12").rpcService(request);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		System.out.println("end:" + Thread.currentThread() + (System.currentTimeMillis()-time));
	}
}
