package com.shockweb.service.ioc.impl.spring;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.shockweb.rpc.spring.ScanClass12;

public class DefaultServiceSpringClient implements Runnable{
	
	ClassPathXmlApplicationContext ctx = null;
	
	public DefaultServiceSpringClient(ClassPathXmlApplicationContext ctx){
		this.ctx = ctx;
	}
	public void run(){
		ScanClass12 scanClass12 = (ScanClass12)ctx.getBean("ScanClass12");
		long time = System.currentTimeMillis();
		for(int i=0;i<10000;i++){
			try{
				scanClass12.print();
			}catch(Exception e){
				e.printStackTrace();
			}

		}
		System.out.println("end:" + Thread.currentThread() + (System.currentTimeMillis()-time));
	}
}
