package com.shockweb.service.ioc.impl.spring;


import java.util.ArrayList;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;


import com.shockweb.rpc.RpcManager;
import com.shockweb.rpc.config.ClientConfig;
public class ScanTest3 {
	

	static List<Thread> lists = new ArrayList<Thread>();
	
	 public static void main(String[] args) throws Exception {
		 int threadCount = 20;
		 ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext2.xml");
		 ClientConfig config = new ClientConfig();
		 config.setRegisterServerUrls("192.168.9.238:3000");
		 config.setRegisterCenterKey("238");
		 RpcManager.addRegisterCenter(config);
		 System.err.println(RpcManager.getClientManager().getRegisterClient().queryServices());
		 long time = System.currentTimeMillis();
		 for(int i=0;i<threadCount;i++){
			 lists.add(new Thread(new ByteServiceByteClient(ctx,new byte[200])));
			 lists.get(i).start();
		 }
		 boolean stop = false;
		 while(!stop){
			 stop = true;
			 for(Thread t:lists){
			 	 if(t.isAlive()){
					 stop = false;
					 break;
				 }
			 }
			 try {
				 Thread.sleep(100);
			 } catch (InterruptedException e) {
				 // TODO Auto-generated catch block
				 e.printStackTrace();
			 }
		 }
		 lists.clear();
		 System.err.println("byte[] Service+byte[] Client:" + (System.currentTimeMillis()-time));
		 RpcManager.close();
	 }
}