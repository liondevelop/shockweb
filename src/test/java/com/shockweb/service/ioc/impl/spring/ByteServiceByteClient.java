package com.shockweb.service.ioc.impl.spring;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.shockweb.rpc.ClientManager;
import com.shockweb.rpc.RpcManager;

public class ByteServiceByteClient implements Runnable{
	
	ClassPathXmlApplicationContext ctx = null;
	byte[] data;
	public ByteServiceByteClient(ClassPathXmlApplicationContext ctx,byte[] data){
		this.ctx = ctx;
		this.data = data;
	}
	public void run(){
		long time = System.currentTimeMillis();
		for(int i=0;i<10000;i++){
			try{
				RpcManager.getClientManager().getClient("default", "ScanClass12").rpcService("ScanClass12", "test", data);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		System.out.println("end:" + Thread.currentThread() + (System.currentTimeMillis()-time));
	}
}
