/*
 *
 * Copyright (c) 2017, baiwang.com. All rights reserved.
 * All rights reserved.
 * Author: zhengb
 * Created: 2017/01/11
 * Description:
 *
 */

package com.shockweb.service;

import com.shockweb.service.ServiceServer;
/**
 * 独立启动
 * 
 * @author 彭明华
 * 2017年12月14日 创建
 */
public class ServiceConfigCenterStartup3301 {

    public static void main(String[] args) throws Exception {
    	ServiceServer.main(new String[]{"127.0.0.1:2000,127.0.0.1:2002", "service","127.0.0.1:3301"});
    }
}
