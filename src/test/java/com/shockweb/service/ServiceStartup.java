/*
 *
 * Copyright (c) 2017, baiwang.com. All rights reserved.
 * All rights reserved.
 * Author: zhengb
 * Created: 2017/01/11
 * Description:
 *
 */

package com.shockweb.service;

import com.shockweb.common.log.LogManager;
import com.shockweb.service.ServiceServer;
import com.shockweb.service.data.ServiceStatus;
/**
 * 独立启动
 * 
 * @author 彭明华
 * 2017年12月14日 创建
 */
public class ServiceStartup {
	

    

    public static void main(String[] args) throws Exception {

    	ServiceServer.start("classpath:config.properties");
    }
}
